# -*- coding: utf-8 -*-

from collections import defaultdict
from model.Mirna import Mirna

__author__ = 'lg'


class Tag(object):
	"""	Class holding a tag """

	__tags_count = defaultdict(int)  # number of tags in each sample, intialized to 0

	def __init__(self, index, seq, qual, count, iso, isite, mirna):
		self.__index = index
		self.__sequence = seq
		self.__quality = qual
		self.__count = count  # number of times this tag has been found
		self.__alignment_mode = defaultdict()  # (size_type, val); size_type: size_ungapped, size_ungapped_1, size_ungapped_2
		self.__alignment_score = defaultdict()  # (score_type, val); score_type: align_score, mir_tag_size_diff, mir2-mir1_align_score
		self.__ambiguity = defaultdict()  # (index, value); index: num_mir_family, num_of_mapped_miRNAs
		self.__isoform = iso
		self.__interaction_site = isite
		self.__begin_ungapped_mirna = None
		self.__begin_ungapped_tag = None
		self.__mirna = mirna
		#Tag.__tags_count[sample.get_name()] += 1
		return

	# @classmethod
	# def get_tags_count(cls, sample_name=None):
	# 	if sample_name is None:
	# 		return cls.__tags_count
	# 	else:
	# 		return cls.__tags_count[sample_name]

	def set_index(self, index):
		self.__index = index
		return

	def set_sequence(self, sequence):
		self.__sequence = sequence
		return

	def set_quality(self, quality):
		self.__quality = quality
		return

	def set_count(self, count):
		self.__count = count
		return

	def add_alignment_mode(self, key, value):
		if key in ("size_ungapped", "size_ungapped_1", "size_ungapped_2"):
			self.__alignment_mode[key] = value
		else:
			raise Exception("Invalid alignment mode key")
		return

	def add_alignment_score(self, key, value):
		if key in ("align_score", "mir_tag_size_diff", "mir2-mir2_align_score"):
			self.__alignment_score[key] = value
		else:
			raise Exception("Invalid score key")
		return

	def add_ambiguity(self, key, value):
		if key in ("num_mir_family" or key == "num_of_mapped_miRNAs"):
			self.__ambiguity[key] = value
		else:
			raise Exception("Invalid ambiguity key")
		return

	def set_isoform(self, value):
		self.__isoform = value
		return

	def set_interaction_site(self, value):
		self.__interaction_site = value
		return

	def set_begin_ungapped_mirna(self, begin_ungapped_mirna):
		self.__begin_ungapped_mirna = begin_ungapped_mirna
		return

	def set_begin_ungapped_tag(self, begin_ungapped_tag):
		self.__begin_ungapped_tag = begin_ungapped_tag
		return

	def set_mirna(self, mirna):
		if isinstance(mirna, Mirna):
			self.__mirna = mirna
		else:
			raise Exception("Not a Mirna")
		return

	def get_index(self):
		return self.__index

	def get_sequence(self):
		return self.__sequence

	def get_quality(self):
		return self.__quality

	def get_count(self):
		return self.__count

	#def get_sample(self):
	#	return self.__sample

	def get_alignment_modes(self):
		return self.__alignment_mode

	def get_alignemnt_scores(self):
		return self.__alignment_score

	def get_ambiguity(self):
		return self.__ambiguity

	def get_isoform(self):
		return self.__isoform

	def get_interaction_site(self):
		return self.__interaction_site

	def get_begin_ungapped_mirna(self):
		return self.__begin_ungapped_mirna

	def get_begin_ungapped_tag(self):
		return self.__begin_ungapped_tag

	def get_mirna(self):
		return self.__mirna
