# -*- coding: utf-8 -*-

from collections import deque, defaultdict
import thread
import threading
from model import Mirna
from model.Sample import Sample

__author__ = 'lg'

class mirnaThread(threading.Thread):
	def __init__(self, samples):
		threading.Thread.__init__(self)
		self.__samples = samples
		self.__samples_by_mirna = defaultdict()
	def run(self):
		print "Starting thread"
		for sample in self.__samples.values():
			tags = sample.get_tags()
			for tag in tags:
				m_name = tag.get_mirna().get_hsa_name()
				if m_name not in self.__samples_by_mirna.keys():
					self.__samples_by_mirna[m_name] = deque()
				if sample not in self.__samples_by_mirna[m_name]:
					self.__samples_by_mirna[m_name].append(sample)
		print "Exiting thread"
	def get_map(self):
		return self.__samples_by_mirna


class Dataset(object):
	""" Class holding the collection of samples of a given dataset """

	__dataset_count = 0

	def __init__(self, name):
		self.__name = name
		self.__samples = defaultdict(Sample)
		self.__samples_by_mirna = None
		self.__isoforms_count = None
		self.__interaction_sites_count = None
		Dataset.__dataset_count += 1
		return

	@classmethod
	def get_dataset_count(cls):
		return cls.__dataset_count

	def __len__(self):
		return self.__samples.__len__()

	def get_dataset_name(self):
		return self.__name

	def __str__(self):
		return "# samples in " + self.__name + ": " + str(len(self.__samples))

	def add_sample(self, sample):
		if isinstance(sample, Sample):
			self.__samples[sample.get_name()] = sample
		else:
			raise Exception("Not a Sample")
		return

	def add_samples(self, samples):
		for sample in samples:
			self.__samples[sample.get_name()] = sample
		# self.__samples.extend(samples)

	def get_samples(self):
		return self.__samples

	def __count_isoforms(self):
		self.__isoforms_count = defaultdict(int)
		for sample in self.__samples.values():
			isoforms_count = sample.get_isoforms_count()
			for isoform in isoforms_count.keys():
				# print isoforms_count[isoform]
				self.__isoforms_count[isoform] += isoforms_count[isoform]

	def __count_interaction_sites(self):
		self.__interaction_sites_count = defaultdict(int)
		for sample in self.__samples.values():
			interaction_sites_count = sample.get_interaction_sites_count()
			for interaction_site in interaction_sites_count.keys():
				# print interaction_sites_count[interaction_site]
				self.__interaction_sites_count[interaction_site] += interaction_sites_count[interaction_site]

	def get_isoforms_count(self):
		if self.__isoforms_count is None:
			self.__count_isoforms()
		return self.__isoforms_count

	def get_interaction_sites_count(self):
		if self.__interaction_sites_count is None:
			self.__count_interaction_sites()
		return self.__interaction_sites_count

	#def add_mirna(self, mirna):
	#	self.__mirnas[mirna.get_hsa_name()] = mirna
	#	return

	def __build_samples_by_mirna_map(self):

		if self.__samples_by_mirna is None:
			try:
				t = mirnaThread(self.__samples)
				t.start()
				t.join()
				self.__samples_by_mirna = t.get_map()
			except:
				print "Error: unable to start thread"
				raise Exception("Error: unable to start thread")

	def get_mirnas(self):
		self.__build_samples_by_mirna_map()
		return self.__samples_by_mirna.keys()

	def get_samples_by_mirna(self, mirna_hsa_name=None):
		self.__build_samples_by_mirna_map()

		if mirna_hsa_name is None:
			return self.__samples_by_mirna
		else:
			return self.__samples_by_mirna[mirna_hsa_name]
