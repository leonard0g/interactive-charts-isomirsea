# -*- coding: utf-8 -*-

from collections import defaultdict, deque
from model import Constants

__author__ = 'lg'

class Mirna(object):
	"""	Class holding a miRNA """

	__mirnas_count = 0

	def __init__(self, mid, hsa_name, mimat_name, seq, seed_index):
		self.__id = mid
		self.__hsa_name = hsa_name
		self.__mimat_name = mimat_name
		self.__seq = seq
		self.__seed_index = seed_index
		Mirna.__mirnas_count += 1
		return

	@classmethod
	def get_mirnas_count(cls):
		return cls.__mirnas_count

	def get_id(self):
		return self.__id

	def get_hsa_name(self):
		return self.__hsa_name

	def get_mimat_name(self):
		return self.__mimat_name

	def get_name(self):
		return self.__hsa_name + " " + self.__mimat_name

	def get_seq(self):
		return self.__seq

	def get_seed_index(self):
		return self.__seed_index