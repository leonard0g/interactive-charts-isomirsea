# -*- coding: utf-8 -*-

from collections import defaultdict, deque
from model.Tag import Tag

__author__ = 'lg'


class Sample(object):
	"""	Class holding a sample """

	__samples_count = 0  # number of samples

	def __init__(self, name, dataset):
		self.__name = name
		self.__tags = deque()
		self.__info = defaultdict()  # dataset level information; see sample_specs3
		self.__mapped_reads_count = 0
		self.__raw_count = 0
		self.__adapter_removed_count = 0
		self.__tags_count = 0
		self.__tags_count_by_mirna_and_isoform = None
		self.__tags_count_by_mirna_and_interaction_site = None

		Sample.__samples_count += 1
		return

	@classmethod
	def get_samples_count(cls):
		return cls.__samples_count

	def add_tag(self, tag):
		if isinstance(tag, Tag):
			self.__tags.append(tag)
		else:
			raise Exception("Not a Tag")
		return

	def set_info(self, info):
		self.__info = info
		return

	def set_dataset(self, dataset):
		from model.Dataset import Dataset
		if isinstance(dataset, Dataset):
			self.__dataset = dataset
		else:
			raise Exception("Not a Dataset")

	def set_mapped_reads(self, count):
		self.__mapped_reads = count

	def get_mapped_reads(self):
		return self.__mapped_reads

	def set_raw(self, count):
		self.__raw_count = count

	def get_raw(self):
		return self.__raw_count

	def set_adapter_removed(self, count):
		self.__adapter_removed_count = count

	def get_adapter_removed(self):
		return self.__adapter_removed_count

	def get_tags(self):
		return self.__tags

	def get_tags_count(self):
		if self.__tags_count is 0:
			for tag in self.__tags:
				self.__tags_count += int(tag.get_count())
		return self.__tags_count

	def get_dataset(self):
		return self.__dataset

	def get_info(self):
		return self.__info

	def get_name(self):
		return self.__name

	def __count_isoforms(self):
		if self.__tags_count_by_mirna_and_isoform is None:
			self.__tags_count_by_mirna_and_isoform = defaultdict(int)
			for tag in self.__tags:
				self.__tags_count_by_mirna_and_isoform[tag.get_mirna().get_hsa_name(), tag.get_isoform()] += int(tag.get_count())

	def __count_interaction_sites(self):
		if self.__tags_count_by_mirna_and_interaction_site is None:
			self.__tags_count_by_mirna_and_interaction_site = defaultdict(int)
			for tag in self.__tags:
				self.__tags_count_by_mirna_and_interaction_site[tag.get_mirna().get_hsa_name(), tag.get_interaction_site()] += int(tag.get_count())

	def get_count_by_mirna_and_isoform(self):
		self.__count_isoforms()
		return self.__tags_count_by_mirna_and_isoform

	def get_count_by_mirna_and_interaction_site(self):
		self.__count_interaction_sites()
		return self.__tags_count_by_mirna_and_interaction_site

	def get_isoforms_count(self, mirna_hsa_name=None):
		self.__count_isoforms()
		if mirna_hsa_name is None:
			res = defaultdict(int)
			for isoform in set(key[1] for key in self.__tags_count_by_mirna_and_isoform.keys()):
				res[isoform] = sum(self.__tags_count_by_mirna_and_isoform[key] for key in self.__tags_count_by_mirna_and_isoform.keys() if key[1] == isoform)
			return res
		else:
			return dict((key[1], self.__tags_count_by_mirna_and_isoform[key]) for key in self.__tags_count_by_mirna_and_isoform.keys() if key[0] == mirna_hsa_name)

	def get_isoform_count(self, isoform):
		res = defaultdict(int)
		for mirna in set(key[0] for key in self.__tags_count_by_mirna_and_isoform.keys()):
			count = sum(self.__tags_count_by_mirna_and_isoform[key] for key in self.__tags_count_by_mirna_and_isoform.keys() if key[0] == mirna and key[1] == isoform)
			if count is not 0:
				res[mirna] = count
		return res

	def get_interaction_sites_count(self, mirna_hsa_name=None):
		self.__count_interaction_sites()
		if mirna_hsa_name is None:
			res = defaultdict(int)
			for interaction_site in set(key[1] for key in self.__tags_count_by_mirna_and_interaction_site.keys()):
				res[interaction_site] = sum(self.__tags_count_by_mirna_and_interaction_site[key] for key in self.__tags_count_by_mirna_and_interaction_site.keys() if key[1] == interaction_site)
			return res
		else:
			return dict((key[1], self.__tags_count_by_mirna_and_interaction_site[key]) for key in self.__tags_count_by_mirna_and_interaction_site.keys() if key[0] == mirna_hsa_name)

	def get_interaction_site_count(self, isite):
		res = defaultdict(int)
		for mirna in set(key[0] for key in self.__tags_count_by_mirna_and_interaction_site.keys()):
			count = sum(self.__tags_count_by_mirna_and_interaction_site[key] for key in self.__tags_count_by_mirna_and_interaction_site.keys() if key[0] == mirna and key[1] == isite)
			if count is not 0:
				res[mirna] = count
		return res