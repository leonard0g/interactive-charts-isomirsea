#!/usr/bin/env python
# -*- coding: utf-8 -*-

from PyQt4 import QtGui
import sys
from main.Gui import MyGui

__author__ = 'lg'


def main():

	app = QtGui.QApplication(sys.argv)
	myapp = MyGui()
	myapp.show()
	sys.exit(app.exec_())

if __name__ == '__main__':
	main()
