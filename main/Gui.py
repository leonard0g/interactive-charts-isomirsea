# -*- coding: utf-8 -*-

from ConfigParser import SafeConfigParser, ParsingError
import multiprocessing
import os
from PyQt4 import QtGui, Qt, QtCore
from PyQt4.QtCore import pyqtSlot, QThread
from PyQt4.QtGui import *
import shutil
from main.Charts import Charts
from main.Data import Data
from gui_components.projectF import Ui_MainWindow
from model import Constants

__author__ = 'lg'

class GenericThread(QThread):
	def __init__(self, function, *args, **kwargs):
		QThread.__init__(self)
		QThread.setTerminationEnabled(True)
		self.function = function
		self.args = args
		self.kwargs = kwargs

	def __del__(self):
		self.wait()

	def run(self):
		self.function(*self.args, **self.kwargs)
		return


class MyGui(QtGui.QMainWindow):

	def __init__(self, parent=None):
		QtGui.QMainWindow.__init__(self, parent)
		self.ui = Ui_MainWindow()
		self.ui.setupUi(self)
		self.__center_on_screen()

		self.charts = Charts()
		self.__setup()
		self.__add_handlers()
		self.genericThread = None

	def __center_on_screen (self):
		resolution = QtGui.QDesktopWidget().screenGeometry()
		self.move((resolution.width() / 2) - (self.frameSize().width() / 2), (resolution.height() / 2) - (self.frameSize().height() / 2))

	def __setup(self):
		# Require values
		try:
			self.__parser = SafeConfigParser()
			self.__parser.read(Constants.CONFIG_FILE_NAME)
			print "working folder = ", self.__parser.get(Constants.FOLDERS, Constants.WORKING_DIR)
			print "source folder = ", self.__parser.get(Constants.FOLDERS, Constants.SOURCE_DIR)
			for dataset in self.__parser.options(Constants.DATASETS):
				print dataset
				self.ui.listWidget_datasets.addItem(QListWidgetItem(dataset))
		except ParsingError, err:
			print 'Could not parse:', err

	def __add_handlers(self):
		self.connect(self.ui.actionAdd_dataset, Qt.SIGNAL("activated()"), self.__add_dataset_handler)
		self.connect(self.ui.button_addDataset, Qt.SIGNAL("clicked()"), self.__add_dataset_handler)
		self.connect(self.ui.actionWorking_folder, Qt.SIGNAL("activated()"), self.__set_working_folder_handler)
		self.connect(self.ui.actionSource_folder, Qt.SIGNAL("activated()"), self.__set_source_folder_handler)
		self.ui.listWidget_datasets.itemClicked.connect(self.__on_dataset_clicked_handler)
		self.ui.listWidget_datasets.itemDoubleClicked.connect(self.__on_dataset_double_clicked_handler)
		# self.ui.button_removeDataset.clicked.connect(self.__remove_dataset_handler())
		self.connect(self.ui.button_removeDataset, Qt.SIGNAL("clicked()"), self.__remove_dataset_handler)
		self.ui.listWidget_samples.itemClicked.connect(self.__on_sample_clicked_handler)
		self.ui.listWidget_mirnas.itemClicked.connect(self.__on_mirna_clicked_handler)
		self.ui.lineEdit_searchMirna.textChanged.connect(self.__on_search_mirna_text_changed_handler)
		self.ui.lineEdit_searchSample.textChanged.connect(self.__on_search_sample_text_changed_handler)
		self.ui.button_reset.clicked.connect(self.__on_reset_clicked_handler)
		self.ui.button_showCharts.clicked.connect(self.__on_show_charts_clicked_handler)

	# http://codrspace.com/durden/pyqt-signal-styles/
	def __remove_handlers(self):
		self.disconnect(self.ui.actionAdd_dataset, Qt.SIGNAL("activated()"), self.__add_dataset_handler)
		self.disconnect(self.ui.button_addDataset, Qt.SIGNAL("clicked()"), self.__add_dataset_handler)
		self.disconnect(self.ui.actionWorking_folder, Qt.SIGNAL("activated()"), self.__set_working_folder_handler)
		self.disconnect(self.ui.actionSource_folder, Qt.SIGNAL("activated()"), self.__set_source_folder_handler)
		self.disconnect(self.ui.listWidget_datasets, Qt.SIGNAL("itemClicked()"), self.__on_dataset_clicked_handler)
		# self.ui.listWidget_datasets.itemClicked.disconnect(self.__on_dataset_clicked_handler)
		self.disconnect(self.ui.listWidget_datasets, Qt.SIGNAL("itemDoubleClicked()"), self.__on_dataset_double_clicked_handler)
		# self.ui.listWidget_datasets.itemDoubleClicked.disconnect(self.__on_dataset_double_clicked_handler)
		# self.ui.button_removeDataset.clicked.connect(self.__remove_dataset_handler())
		self.disconnect(self.ui.button_removeDataset, Qt.SIGNAL("clicked()"), self.__remove_dataset_handler)
		self.disconnect(self.ui.listWidget_samples, Qt.SIGNAL("itemClicked()"), self.__on_sample_clicked_handler)
		# self.ui.listWidget_samples.itemClicked.disconnect(self.__on_sample_clicked_handler)
		self.disconnect(self.ui.listWidget_mirnas, Qt.SIGNAL("itemClicked()"), self.__on_mirna_clicked_handler)
		# self.ui.listWidget_mirnas.itemClicked.disconnect(self.__on_mirna_clicked_handler)
		self.disconnect(self.ui.lineEdit_searchMirna, Qt.SIGNAL("textChanged()"), self.__on_search_mirna_text_changed_handler)
		# self.ui.lineEdit_searchMirna.textChanged.disconnect(self.__on_search_mirna_text_changed_handler)
		self.disconnect(self.ui.lineEdit_searchSample, Qt.SIGNAL("textChanged()"), self.__on_search_sample_text_changed_handler)
		# self.ui.lineEdit_searchSample.textChanged.disconnect(self.__on_search_sample_text_changed_handler)
		self.disconnect(self.ui.button_reset, Qt.SIGNAL("clicked()"), self.__on_reset_clicked_handler)
		# self.ui.button_reset.clicked.disconnect(self.__on_reset_clicked_handler)

	def __add_dataset_handler(self):
		print "add dataset handler"
		dataset_folder = QFileDialog.getExistingDirectory(None, self.tr("Choose a dataset folder"), self.__parser.get(Constants.FOLDERS, Constants.SOURCE_DIR), QFileDialog.DontResolveSymlinks)
		if dataset_folder.length() > 0:
			print "dataset ", dataset_folder, " selected"
			self.ui.listWidget_datasets.addItem(QListWidgetItem(os.path.basename(str(dataset_folder))))
			with open(Constants.CONFIG_FILE_NAME, 'w') as fp:
				self.__parser.set(Constants.DATASETS, os.path.basename(str(dataset_folder)), str(dataset_folder))
				self.__parser.write(fp)

	def __set_working_folder_handler(self):
		msgBox = QtGui.QMessageBox()
		msgBox.setText("Current working folder:\n" + os.path.abspath(self.__parser.get(Constants.FOLDERS, Constants.WORKING_DIR)))

		browseButton = msgBox.addButton(self.tr('Browse'), QtGui.QMessageBox.ActionRole)
		msgBox.addButton(QtGui.QMessageBox.Cancel)

		msgBox.exec_()

		if msgBox.clickedButton() == browseButton:
			print "browse"
			working_dir = QFileDialog.getExistingDirectory(None, self.tr("Choose or create a directory"), os.path.abspath(self.__parser.get(Constants.FOLDERS, Constants.WORKING_DIR)), QFileDialog.DontResolveSymlinks)
			if str(working_dir) is not None and str(working_dir).__len__() > 0:
				with open(Constants.CONFIG_FILE_NAME, 'w') as fp:
					self.__parser.set(Constants.FOLDERS, Constants.WORKING_DIR, str(working_dir))
					self.__parser.write(fp)
		else:
			print "cancel"

	def __set_source_folder_handler(self):
		msgBox = QtGui.QMessageBox()
		msgBox.setText("Current source folder:\n" + os.path.abspath(self.__parser.get(Constants.FOLDERS, Constants.SOURCE_DIR)))

		browseButton = msgBox.addButton(self.tr('Browse'), QtGui.QMessageBox.ActionRole)
		msgBox.addButton(QtGui.QMessageBox.Cancel)

		msgBox.exec_()

		if msgBox.clickedButton() == browseButton:
			print "browse"
			source_dir = QFileDialog.getExistingDirectory(None, self.tr("Choose or create a directory"), os.path.abspath(self.__parser.get(Constants.FOLDERS, Constants.SOURCE_DIR)), QFileDialog.DontResolveSymlinks)
			if str(source_dir) is not None and str(source_dir).__len__() > 0:
				with open(Constants.CONFIG_FILE_NAME, 'w') as fp:
					self.__parser.set(Constants.FOLDERS, Constants.SOURCE_DIR, str(source_dir))
					self.__parser.write(fp)
		else:
			print "cancel"

	def __on_dataset_clicked_handler(self, item):
		# Il click singolo viene gestito 2 volte quando c'è un doubleClick...
		print('row %d selected' % self.ui.listWidget_datasets.currentRow())
		self.ui.button_removeDataset.setEnabled(True)
		print "folder ", self.__parser.get(Constants.DATASETS, str(item.text()))

	def __on_dataset_double_clicked_handler(self, item):
		# Il click singolo viene gestito 2 volte quando c'e' un doubleClick...
		print('row %d selected' % self.ui.listWidget_datasets.currentRow())
		if self.genericThread is None or not self.genericThread.isRunning():
			self.ui.listWidget_datasets.clearSelection()
			self.ui.listWidget_samples.clear()
			self.ui.listWidget_mirnas.clear()
			self.__reset_all()
			self.__disable_interface()
			self.genericThread = GenericThread(self.__load_data, item)

			class Progress(QtCore.QObject):
				# create a new signal on the fly and name it
				showProgress = QtCore.pyqtSignal(str)
				closeProgress = QtCore.pyqtSignal()
			self.progress_trigger = Progress()
			self.progress_trigger.showProgress.connect(self.__show_progress)
			self.progress_trigger.closeProgress.connect(self.__close_progress)

			self.progress_trigger.showProgress.emit("Loading dataset " + str(item.text()))
			self.genericThread.start()

	def __load_data(self, item):
		print "folder ", self.__parser.get(Constants.DATASETS, str(item.text()))
		self.data = None
		try:
			self.data = Data(self.__parser.get(Constants.DATASETS, str(item.text())), self.__parser.get(Constants.FOLDERS, Constants.WORKING_DIR))
		except Exception as err:
			self.__enable_interface()
			print err.message

		else:
			samples = self.data.get_dataset().get_samples().keys()
			samples.sort()
			self.ui.listWidget_samples.addItems(samples)
			self.ui.groupBox_samples.setEnabled(True)

			mirnas = self.data.get_dataset().get_mirnas()
			mirnas.sort()
			self.ui.listWidget_mirnas.addItems(mirnas)
			self.ui.groupBox_mirnas.setEnabled(True)

			self.ui.groupBox_datasetCharts.setEnabled(True)
		self.progress_trigger.closeProgress.emit()

	def __remove_dataset_handler(self):
		print "remove dataset"
		list_items = self.ui.listWidget_datasets.selectedItems()
		for item in list_items:
			dir = self.__parser.get(Constants.FOLDERS, os.path.join(Constants.WORKING_DIR), str(item.text()))
			reply = QtGui.QMessageBox.question(self, 'Message', "Remove also the working folder" + "\n" + dir + " ?", QtGui.QMessageBox.Yes, QtGui.QMessageBox.Cancel, QtGui.QMessageBox.No)
			if not reply == QMessageBox.Cancel:
				self.ui.listWidget_datasets.takeItem(self.ui.listWidget_datasets.row(item))

				with open(Constants.CONFIG_FILE_NAME, 'w') as fp:
					self.__parser.remove_option(Constants.DATASETS, str(item.text()))
					self.__parser.write(fp)
					print "dataset ", str(item.text()), "removed"
				if reply == QMessageBox.Yes:
					print "deleting folder"
					shutil.rmtree(os.path.join(self.__parser.get(Constants.FOLDERS, Constants.WORKING_DIR), str(item.text())) + "/", ignore_errors=True)
					print "folder deleted"
				self.__reset_all()
				self.ui.groupBox_datasetCharts.setEnabled(False)
				self.ui.listWidget_samples.clear()
				self.ui.listWidget_mirnas.clear()
			else:
				print "cancel"

	def __on_sample_clicked_handler(self):
		print('row %d selected' % self.ui.listWidget_samples.currentRow())
		self.ui.groupBox_sampleCharts.setEnabled(True)

	def __on_mirna_clicked_handler(self):
		print('row %d selected' % self.ui.listWidget_mirnas.currentRow())
		self.ui.groupBox_mirnaCharts.setEnabled(True)

	def __reset_all(self):
		self.ui.groupBox_sampleCharts.setEnabled(False)
		self.ui.groupBox_mirnaCharts.setEnabled(False)
		self.ui.checkBox_datasetAll.setChecked(False)
		self.ui.checkBox_datasetPiechart.setChecked(False)
		self.ui.checkBox_datasetPiechart_2.setChecked(False)
		self.ui.checkBox_datasetAll.setChecked(False)
		self.ui.checkBox_samplePiechart.setChecked(False)
		self.ui.checkBox_samplePiechart_2.setChecked(False)
		self.ui.checkBox_sampleIsoforms.setChecked(False)
		self.ui.checkBox_mirnaIsoforms.setChecked(False)
		self.ui.listWidget_samples.clearSelection()
		self.ui.listWidget_mirnas.clearSelection()
		self.ui.lineEdit_searchMirna.clear()
		self.ui.lineEdit_searchSample.clear()

	def __on_search_mirna_text_changed_handler(self):
		self.ui.groupBox_mirnaCharts.setEnabled(False)
		query = str(self.ui.lineEdit_searchMirna.text()).lower()
		# print "search mirna query: ", query
		mirnas = self.data.get_dataset().get_mirnas()
		res = filter(lambda k: query in k.lower(), mirnas)
		res.sort()
		self.ui.listWidget_mirnas.clear()
		self.ui.listWidget_mirnas.addItems(res)

	def __on_search_sample_text_changed_handler(self):
		self.ui.groupBox_sampleCharts.setEnabled(False)
		query = str(self.ui.lineEdit_searchSample.text()).lower()
		# print "search sample query: ", query
		samples = self.data.get_dataset().get_samples().keys()
		res = filter(lambda k: query in k.lower(), samples)
		res.sort()
		self.ui.listWidget_samples.clear()
		self.ui.listWidget_samples.addItems(res)

	def __on_reset_clicked_handler(self):
		self.__reset_all()

	@pyqtSlot(str)
	def __show_progress(self, msg):
		print "show progress"
		self.progress_bar = QProgressDialog(self)
		self.progress_bar.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
		self.progress_bar.setMinimumSize(400, 150)
		self.progress_bar.setMaximumSize(400, 150)
		self.progress_bar.setWindowTitle("Please wait...")
		self.progress_bar.setLabelText(msg)
		self.progress_bar.setMinimum(0)
		self.progress_bar.setMaximum(0)
		self.progress_bar.canceled.connect(self.__on_progress_cancel)
		self.progress_bar.show()

	def __on_progress_cancel(self):
		print "on progress cancel"
		if self.genericThread is not None:
			self.genericThread.terminate()
			QThread.wait(QThread(self.genericThread), 0)
			#TODO probably useless busy wait
			# while not self.genericThread.isFinished():
			# 	print "waiting thread to finish"
			self.genericThread = None
		self.progress_trigger = None
		self.__close_progress()

	def __on_show_charts_clicked_handler(self):

		if self.ui.checkBox_datasetAll.isChecked():
			print "dataset all"
			another_thread = multiprocessing.Process(target=self.charts.dataset_summary_chart, args=(self.data.get_dataset(), self.__parser.get(Constants.FOLDERS, Constants.SOURCE_DIR)))
			another_thread.start()

		if self.ui.checkBox_datasetPiechart.isChecked():
			print "dataset isoforms piechart"
			another_thread = multiprocessing.Process(target=self.charts.piechart_dataset, args=(self.data.get_dataset(), Constants.ISOFORMS))
			another_thread.start()

		if self.ui.checkBox_datasetPiechart_2.isChecked():
			print "dataset interaction sites piechart"
			another_thread = multiprocessing.Process(target=self.charts.piechart_dataset, args=(self.data.get_dataset(), Constants.INTERACTION_SITES))
			another_thread.start()

		if self.ui.checkBox_sampleIsoforms.isChecked():
			print "sample isoforms"
			another_thread = multiprocessing.Process(target=self.charts.stacked_barchart_sample_mirnas_breakdown_by_x, args=(self.data.get_dataset(), str(self.ui.listWidget_samples.currentItem().text()), Constants.ISOFORMS))
			another_thread.start()

		if self.ui.checkBox_samplePiechart.isChecked():
			print "sample isoforms piechart"
			another_thread = multiprocessing.Process(target=self.charts.piechart_sample, args=(self.data.get_dataset(), str(self.ui.listWidget_samples.currentItem().text()), Constants.ISOFORMS))
			another_thread.start()

		if self.ui.checkBox_samplePiechart_2.isChecked():
			print "sample interaction sites piechart"
			another_thread = multiprocessing.Process(target=self.charts.piechart_sample, args=(self.data.get_dataset(), str(self.ui.listWidget_samples.currentItem().text()), Constants.INTERACTION_SITES))
			another_thread.start()

		if self.ui.checkBox_mirnaIsoforms.isChecked():
			print "mirna isoforms"
			another_thread = multiprocessing.Process(target=self.charts.stacked_barchart_mirna_samples_isoforms,args=(self.data.get_dataset(), str(self.ui.listWidget_mirnas.currentItem().text())))
			another_thread.start()

	@pyqtSlot()
	def __close_progress(self):
		self.progress_bar.close()
		self.__enable_interface()

	def __disable_interface(self):
		self.selected_dataset = self.ui.listWidget_datasets.currentRow()
		self.ui.listWidget_datasets.setEnabled(False)
		self.ui.listWidget_samples.setEnabled(False)
		self.ui.listWidget_mirnas.setEnabled(False)
		self.ui.groupBox_charts.setEnabled(False)

	def __enable_interface(self):
		self.ui.listWidget_datasets.setEnabled(True)
		self.ui.listWidget_datasets.setCurrentRow(self.selected_dataset)
		self.ui.listWidget_samples.setEnabled(True)
		self.ui.listWidget_mirnas.setEnabled(True)
		self.ui.groupBox_charts.setEnabled(True)