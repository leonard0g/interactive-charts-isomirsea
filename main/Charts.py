# -*- coding: utf-8 -*-

from collections import defaultdict, deque
import collections
from operator import itemgetter
import os
import re
import math
from matplotlib.backend_bases import PickEvent, MouseEvent
from matplotlib.figure import Figure
from matplotlib.font_manager import FontProperties
from matplotlib.legend import Legend
from matplotlib.patches import Rectangle, Wedge
from matplotlib.text import Text, Annotation
from matplotlib.widgets import Button
import matplotlib.pyplot as plt
import numpy as np
from ConfigParser import SafeConfigParser, ParsingError
from model import Constants
from itertools import (takewhile,repeat)

__author__ = 'lg'


class Charts(object):
	"""class holding methods for charts plotting"""

	def __init__(self):
		self.__isoforms = list()
		self.__interaction_sites = list()
		self.__isoform_colors = defaultdict()
		self.__interaction_sites_colors = defaultdict()
		self.__dataset_summary_colors = defaultdict()
		self.__working_folder = None
		self.__export_folder = None

		self.__read_config()

		return

	def __read_config(self):
		try:
			parser = SafeConfigParser()
			parser.read(Constants.CONFIG_FILE_NAME)

			self.__working_folder = parser.get(Constants.FOLDERS, Constants.WORKING_DIR)

			isoforms = list()
			for isoform in parser.options("isoforms"):
				isoforms.append(isoform)
			print "isoforms: ", isoforms
			self.__isoforms = isoforms

			int_sites = list()
			for int_site in parser.options("interaction sites"):
				int_sites.append(int_site)
			print "interaction sites: ", int_sites
			self.__interaction_sites = int_sites

			isoform_colors = defaultdict()
			for isoform in parser.options("isoform colors"):
				isoform_colors[isoform] = parser.get("isoform colors", isoform)
			print "isoform colors: ", isoform_colors
			self.__isoform_colors = isoform_colors

			interaction_site_colors = defaultdict()
			for isite in parser.options("interaction site colors"):
				interaction_site_colors[isite] = parser.get("interaction site colors", isite)
			print "interaction site colors: ", interaction_site_colors
			self.__interaction_sites_colors = interaction_site_colors

			summary_colors = list()
			for key in parser.options("dataset summary colors"):
				summary_colors.append(parser.get("dataset summary colors", key))
			print "dataset summary colors: ", summary_colors
			self.__dataset_summary_colors = summary_colors

		except ParsingError, err:
			print 'Could not parse:', err

	# D0
	def piechart_dataset(self, dataset, what):
		fig = plt.figure(figsize=(Constants.PIECHART_W, Constants.PIECHART_H))
		plt.subplots_adjust(left=Constants.LEFT_ADJUST, top=Constants.TOP_ADJUST)
		rect = 0.1, 0.15, 0.6, 0.8
		ax = fig.add_axes(rect)
		plt.axis('equal')
		angle = 90

		plt.title(dataset.get_dataset_name() + " " + what + " %")
		fig.canvas.set_window_title(dataset.get_dataset_name() + " " + what + " %")

		if what == Constants.ISOFORMS:
			data_count = dataset.get_isoforms_count()
			colors = [self.__isoform_colors[k] for k in data_count.keys()]
		else:
			data_count = dataset.get_interaction_sites_count()
			colors = [self.__interaction_sites_colors[k] for k in data_count.keys()]

		total = sum(data_count.itervalues())
		labels = ['{0} - {1:1.2f} %'.format(i, 100.0 * data_count[i] / total) for i in data_count.keys()]
		sizes = data_count.values()
		wedges, texts, autotexts = ax.pie(sizes, colors=colors, autopct='%1.2f%%', startangle=angle)
		for wedge in wedges:
			wedge.set_edgecolor('white')

		self.piechart_autotext(wedges, autotexts)

		p, l, __ = zip(*sorted(zip(wedges, labels, sizes), key=lambda k: k[2], reverse=True))
		legend = fig.legend(p, l, loc="upper right")
		legend.draggable(state=True)

		patches_with_annotation = dict()
		self.annotate_patches(wedges, labels, patches_with_annotation)

		def on_mouse_over(event):
			self.piechart_on_mouse_over(event, patches_with_annotation)
		fig.canvas.mpl_connect('motion_notify_event', on_mouse_over)

		plt.show()

	# D1
	def dataset_summary_chart(self, dataset, source_dir):
		colors = self.__dataset_summary_colors
		mapped_reads = dict()
		raw_data = dict()
		ar_data = dict()
		samples = dataset.get_samples()
		for sample in samples.values():
			mapped_reads[sample.get_name()] = sample.get_mapped_reads()
			raw_data[sample.get_name()] = sample.get_raw()
			ar_data[sample.get_name()] = sample.get_adapter_removed()

		width = Constants.BAR_WIDTH
		tickpositions = np.arange(len(samples))
		ylim = 10**(np.ceil(np.log10(np.amax(raw_data.values()))))
		xlim = len(samples) - width

		if len(samples) <= 50:
			figsize=(Constants.FIGURE_W_S, Constants.FIGURE_H)
		else:
			figsize = (Constants.FIGURE_W, Constants.FIGURE_H)
		fig = plt.figure(figsize=figsize)

		ax1 = fig.add_subplot(221)
		ax1_rects = ax1.bar(tickpositions, raw_data.values(), width, color=colors[0], edgecolor='white', align='center')
		ax1.set_xticks(tickpositions)
		ax1.set_xticklabels(raw_data.keys(), size='small', rotation=40, ha='right')
		ax1.set_ylim(0, ylim)
		ax1.set_xlim(-width, xlim)
		ax1.set_title("RAW data")

		ax2 = fig.add_subplot(222)
		ax2_rects = ax2.bar(tickpositions, ar_data.values(), width, color=colors[1], edgecolor='white', align='center')
		ax2.set_xticks(tickpositions)
		ax2.set_xticklabels(ar_data.keys(), size='small', rotation=40, ha='right')
		ax2.set_ylim(0, ylim)
		ax2.set_xlim(-width, xlim)
		ax2.set_title("Adapter removed")

		ax3 = fig.add_subplot(223)
		ax3_rects = ax3.bar(tickpositions, mapped_reads.values(), width, color=colors[2], edgecolor='white', align='center')
		ax3.set_xticks(tickpositions)
		ax3.set_xticklabels(mapped_reads.keys(), size='small', rotation=40, ha='right')
		ax3.set_ylim(0, ylim)
		ax3.set_xlim(-width, xlim)
		ax3.set_title("Mapped reads")

		ax4 = fig.add_subplot(224)
		ax4_rects_1 = ax4.bar(tickpositions - 0.2, raw_data.values(), width=Constants.BAR_WIDTH_S, color=colors[0], edgecolor='white', align='center')
		ax4_rects_2 = ax4.bar(tickpositions, ar_data.values(), width=Constants.BAR_WIDTH_S, color=colors[1], edgecolor='white', align='center')
		ax4_rects_3 = ax4.bar(tickpositions + 0.2, mapped_reads.values(), width=Constants.BAR_WIDTH_S, color=colors[2], edgecolor='white', align='center')
		ax4.set_xticks(tickpositions)
		ax4.set_xticklabels(mapped_reads.keys(), size='small', rotation=40, ha='right')
		ax4.set_ylim(0, ylim)
		ax4.set_xlim(-width, xlim)
		ax4.set_title("RAW + adapter removed + mapped reads")

		patches_with_annotation = []
		def annotate(axes, r, text):
			if type(text) is tuple:
				text = "\n".join("{0}".format(n) for n in text)
			annotation = axes.annotate(xy=(r.get_x() - width/2, r.get_height()/2), s=text, horizontalalignment="left", verticalalignment="baseline", bbox=dict(boxstyle="round", facecolor="w", edgecolor="0.5", alpha=0.9))
			annotation.set_visible(False)
			patches_with_annotation.append([r, annotation])

		for rect, label in zip(ax1_rects, raw_data.values()):
			annotate(ax1, rect, label)
		for rect, label in zip(ax2_rects, ar_data.values()):
			annotate(ax2, rect, label)
		for rect, label in zip(ax3_rects, mapped_reads.values()):
			annotate(ax3, rect, label)
		for rect, label in zip(ax4_rects_1, zip(raw_data.values(), ar_data.values(), mapped_reads.values())):
			annotate(ax4, rect, label)
		for rect, label in zip(ax4_rects_2, zip(raw_data.values(), ar_data.values(), mapped_reads.values())):
			annotate(ax4, rect, label)
		for rect, label in zip(ax4_rects_3, zip(raw_data.values(), ar_data.values(), mapped_reads.values())):
			annotate(ax4, rect, label)

		def on_mouse_over(event):
			visibility_changed = False
			for patch, annotation in patches_with_annotation:
				should_be_visible = (patch.contains(event)[0] == True)
				if should_be_visible != annotation.get_visible():
					visibility_changed = True
					annotation.set_visible(should_be_visible)
			if visibility_changed:
				plt.draw()
		fig.canvas.mpl_connect('motion_notify_event', on_mouse_over)

		fig.subplots_adjust(bottom=Constants.BOTTOM_ADJUST, right=Constants.RIGHT_ADJUST)
		plt.tight_layout(pad=1.)
		fig.canvas.set_window_title(dataset.get_dataset_name() + " reads overview")
		plt.show()

	#S0
	def piechart_sample(self, dataset, sample_name, what):
		fig, ax = plt.subplots(figsize=(Constants.PIECHART_W, Constants.PIECHART_H))
		plt.subplots_adjust(left=Constants.LEFT_ADJUST, top=Constants.TOP_ADJUST)
		plt.axis('equal')
		angle = 90

		sample = dataset.get_samples()[sample_name]
		if what == Constants.ISOFORMS:
			data_count = sample.get_isoforms_count()
			data_by_mirna = sample.get_count_by_mirna_and_isoform()  # for S2
			fig.canvas.set_window_title(dataset.get_dataset_name() + ", " + sample_name + " " + Constants.S0_ISOFORMS_TITLE)
			button_title = Constants.S0_SHOW_S1_BUTTON_ISO
			colors = [self.__isoform_colors[k] for k in data_count.keys()]
		else:
			data_count = sample.get_interaction_sites_count()
			data_by_mirna = sample.get_count_by_mirna_and_interaction_site()  # for S2
			fig.canvas.set_window_title(dataset.get_dataset_name() + ", " + sample_name + " " + Constants.S0_INTERACTION_SITES_TITLE)
			button_title = Constants.S0_SHOW_S1_BUTTON_INT_SITE
			colors = [self.__interaction_sites_colors[k] for k in data_count.keys()]

		total = sum(data_count.itervalues())
		labels = ['{0} - {1:1.2f} %'.format(i, 100.0 * data_count[i] / total) for i in data_count.keys()]
		sizes = data_count.values()

		# for S2
		keys = self.__isoforms if what == Constants.ISOFORMS else self.__interaction_sites
		mirna_labels = list(set([key[0] for key in data_by_mirna.keys()]))
		array_dictionary = Charts.get_mirnas_breakdown_by_x(keys, mirna_labels, data_by_mirna)
		s2_totals = np.asarray(array_dictionary.values()).sum(axis=0)

		wedges, texts, autotexts = plt.pie(sizes, colors=colors, autopct='%1.2f%%', startangle=angle)

		self.piechart_autotext(wedges, autotexts)

		p, l, __ = zip(*sorted(zip(wedges, labels, sizes), key=lambda k: k[2], reverse=True))
		legend = ax.legend(p, l, bbox_to_anchor=(1, 1))
		legend.draggable(state=True)
		plt.title(sample_name)

		patches_with_annotation = dict()
		self.annotate_patches(wedges, labels, patches_with_annotation)

		legend_patches = defaultdict()
		for label, patch in zip(legend.get_texts(), legend.get_patches()):
			label.set_picker(5)
			legend_patches[patch] = label.get_text()
			patch.set_picker(5)  # prende solo dei Rectangle di dimensioni x, y senza altre info...

		for wedge in wedges:
			wedge.set_edgecolor('white')
			wedge.set_picker(5)

		def on_mouse_over(event):
			self.piechart_on_mouse_over(event, patches_with_annotation)

		def on_button_clicked(event):
			print event.__class__
			self.stacked_barchart_sample_mirnas_breakdown_by_x(dataset, sample_name, what)  # S1

		axnext = plt.axes([0.6, 0.2, 0.3, 0.075])  # right, bottom, width, height
		button = Button(axnext, button_title, color='0.85', hovercolor='0.95')
		button.on_clicked(on_button_clicked)

		def on_pick(event):
			_a = event.artist
			_label = None
			if isinstance(_a, Legend):
				for r in _a.hitlist(event.mouseevent):
					if isinstance(r, Rectangle) and r in legend_patches.keys():
						_label = re.search("(.*?) - ", legend_patches[r]).group(1)
						break
					elif isinstance(r, Text) and r.get_text() in legend_patches.values():
						_label = re.search("(.*?) - ", r.get_text()).group(1)
						break
			elif isinstance(_a, Wedge):
				_label = re.search("(.*?) - ", patches_with_annotation[_a].get_text()).group(1)

			if _label is not None:
				print sample_name + ", " + _label
				#  this trick is for the legend to be released
				legend.draggable(state=False)
				self.barchart_sample_mirnas_by_x(dataset, sample, array_dictionary[_label], mirna_labels, s2_totals, _label, what)  # S2
				legend.draggable(state=True)

		fig.canvas.mpl_connect('motion_notify_event', on_mouse_over)
		legend.figure.canvas.mpl_connect('pick_event', on_pick)

		plt.show()

	def piechart_autotext(self, wedges, autotexts):
		for patch, txt in zip(wedges, autotexts):
			# the angle at which the text is located
			ang = (patch.theta2 + patch.theta1) / 2.
			# new coordinates of the text, 0.8 is the distance from the center
			x = patch.r * 0.8 * np.cos(ang * np.pi / 180)
			y = patch.r * 0.8 * np.sin(ang * np.pi / 180)
			# if patch is too narrow, hide the text
			if (patch.theta2 - patch.theta1) < 5.:
				txt.set_text('')
			# if patch is narrow enough, move text to new coordinates
			else:
				if (patch.theta2 - patch.theta1) < 20.:
					txt.set_position((x, y))

	def annotate_patches(self, wedges, labels, patches_with_annotation):
		for rect, label in zip(wedges, labels):
			ang = (rect.theta2 + rect.theta1) / 2.
			x = rect.r * 1.1 * np.cos(ang * np.pi / 180)
			y = rect.r * 1.1 * np.sin(ang * np.pi / 180)
			annotation = plt.annotate(xy=(x, y), s=label, horizontalalignment="center", verticalalignment="baseline", bbox=dict(boxstyle="round", facecolor="w", edgecolor="0.5", alpha=0.9))
			annotation.set_visible(False)
			patches_with_annotation[rect] = annotation

	def piechart_on_mouse_over(self, event, patches_with_annotation):
		visibility_changed = False
		for patch in patches_with_annotation.keys():
			should_be_visible = (patch.contains(event)[0] == True)
			annotation = patches_with_annotation[patch]
			if should_be_visible != annotation.get_visible():
				visibility_changed = True
				annotation.set_visible(should_be_visible)
		if visibility_changed:
			plt.draw()

	@staticmethod
	def __get_figwidth_rect(nticks):
		fig_width = 0
		fw = min(100, max(15, 0.3 * (nticks + 35)))
		if fw == 100:
			fig_width = 100
			rect = 0.015, 0.15, 0.93, 0.8
		elif fw < 100 and fw >= 80:
			fig_width = 80
			rect = 0.022, 0.15, 0.925, 0.8
		elif fw < 80 and fw >= 50:
			fig_width = 65
			rect = 0.03, 0.15, 0.88, 0.8
		elif fw < 50 and fw >= 30:
			fig_width = 40
			rect = 0.07, 0.15, 0.8, 0.8
		elif fw < 30 and fw >= 20:
			fig_width = 25
			rect = 0.07, 0.15, 0.7, 0.8
		else:
			fig_width = 15
			rect = 0.1, 0.15, 0.5, 0.8
		return fig_width, rect

	# S2
	def barchart_sample_mirnas_by_x(self, dataset, sample, count_by_mirna, mirna_labels, totals, label, what):
		#plt.ion()
		if what == Constants.ISOFORMS:
			color = self.__isoform_colors[label]
		else:
			color = self.__interaction_sites_colors[label]

		mirna_labels, count_by_mirna, totals = zip(*[(m, c, t) for m, c, t in zip(mirna_labels, count_by_mirna, totals) if c > 0])

		labels = mirna_labels

		fig_width, rect = Charts.__get_figwidth_rect(len(labels))
		fig = plt.figure(figsize=(fig_width, Constants.FIGURE_H))
		ax = fig.add_axes(rect)

		tickpositions = np.arange(len(labels))
		bar_width = Constants.BAR_WIDTH
		xlim = len(labels) - bar_width
		ax.set_xlim(-bar_width, xlim)
		ax.set_xticks(tickpositions)
		ax.set_xticklabels(labels, size='small', rotation=40, ha='right', picker=True)

		relative_data = [m * 100.0 / n for m, n in zip(count_by_mirna, totals)]
		p = ax.bar(tickpositions, relative_data, width=bar_width, color=color, edgecolor='white', align='center')

		legend = ax.legend([p[0]], [label], bbox_to_anchor=(1, 1), loc=2)
		legend.draggable(state=True)

		ax.set_ylabel("Percent (%)")
		ax.set_ylim(0, max(relative_data) * 1.1)
		ax.yaxis.set_ticks_position('left')

		plt.title("sample " + sample.get_name() + ", " + label)
		fig.canvas.set_window_title("sample " + sample.get_name() + ", " + label)
		fig.autofmt_xdate()

		# prevents picking overlapping labels with the same click
		class Fscope:
			xy = list()
		def onpick(event):
			xy = (event.mouseevent.x, event.mouseevent.y)
			if xy not in Fscope.xy:
				Fscope.xy.append(xy)
				_a = event.artist
				if isinstance(_a, Text):
					_selected = _a.get_text()
					if _selected in labels:
						self.table_sample_tag_quality(dataset, sample.get_name(), _selected, what, label)  # _selected == mirna
						return
				else:
					#  this trick is for the legend to be released
					legend.draggable(state=False)
					legend.draggable(state=True)

		pick_connection_id = ax.figure.canvas.mpl_connect('pick_event', onpick)

		#plt.draw()
		#plt.ioff()
		plt.show()

	@staticmethod
	def get_mirnas_breakdown_by_x(keys, mirna_labels, data_by_mirna):
		array_dictionary = defaultdict(list)
		for key in keys:
			for mirna in mirna_labels:
				array_dictionary[key].append(int(data_by_mirna[mirna, key]))
		return array_dictionary

	# S1
	def stacked_barchart_sample_mirnas_breakdown_by_x(self, dataset, sample_name, what):
		plt.ioff()
		sample = dataset.get_samples()[sample_name]
		if what == Constants.ISOFORMS:
			data_by_mirna = sample.get_count_by_mirna_and_isoform()
			keys = list(set([key[1] for key in data_by_mirna.keys()]))
			colors = self.__isoform_colors
		else:
			data_by_mirna = sample.get_count_by_mirna_and_interaction_site()
			keys = list(set([key[1] for key in data_by_mirna.keys()]))
			colors = self.__interaction_sites_colors

		mirna_labels = list(set([key[0] for key in data_by_mirna.keys()]))

		array_dictionary = Charts.get_mirnas_breakdown_by_x(keys, mirna_labels, data_by_mirna)

		# sum over columns
		totals = np.asarray(array_dictionary.values()).sum(axis=0)
		height_cumulative = np.zeros(len(mirna_labels))

		fig_width, rect = Charts.__get_figwidth_rect(len(mirna_labels))
		fig_height = Constants.FIGURE_H_S
		fig = plt.figure(figsize=(fig_width, fig_height))
		ax = fig.add_axes(rect)

		tickpositions = np.arange(len(mirna_labels))
		bar_width = Constants.BAR_WIDTH
		xlim = len(mirna_labels) - bar_width
		ax.set_xlim(-bar_width, xlim)
		ax.set_ylim(0, 110)

		bars = defaultdict()
		for i in xrange(0, len(keys)):
			relative_data = [m * 100.0 / n for m, n in zip(array_dictionary[keys[i]], totals)]
			bars[i] = ax.bar(tickpositions, relative_data, bottom=height_cumulative, width=bar_width, color=colors[keys[i]], edgecolor='white', align='center')
			height_cumulative += relative_data
		plt.xticks(tickpositions, mirna_labels, fontsize=10, picker=True)

		fig.canvas.set_window_title("sample " + sample_name + " miRNAs, breakdown by " + what)
		plt.title("sample " + sample_name + " miRNAs, breakdown by " + what)
		fig.autofmt_xdate()
		legend = plt.legend([p[0] for p in bars.values()], keys, bbox_to_anchor=(1, 1), loc=2)
		legend.draggable(state=True)

		legend_patches = defaultdict()
		for label, patch in zip(legend.get_texts(), legend.get_patches()):
			legend_patches[patch] = label.get_text()

		# prevents picking overlapping labels with the same click
		class Fscope:
			xy = list()
		def onpick(event):
			_selected = None

			if event.artist is None:
				return

			_a = event.artist
			if isinstance(_a, Legend):
				for r in _a.hitlist(event.mouseevent):
					if isinstance(r, Rectangle) and r in legend_patches.keys():
						_selected = legend_patches[r]
						break
					elif isinstance(r, Text) and r.get_text() in legend_patches.values():
						_selected = r.get_text()
						break
				if _selected is not None:
					#  this trick is for the legend to be released
					legend.draggable(state=False)
					self.barchart_sample_mirnas_by_x(dataset, sample, array_dictionary[_selected], mirna_labels, totals, _selected, what)
					legend.draggable(state=True)
					return

			elif isinstance(_a, Text) and _a.get_text() in mirna_labels:
				xy = (event.mouseevent.x, event.mouseevent.y)
				if xy not in Fscope.xy:
					Fscope.xy.append(xy)
					_selected = _a.get_text()
					self.table_sample_tag_quality(dataset, sample_name, _selected)  # _selected == mirna
					return

		pick_connection_id = fig.canvas.mpl_connect('pick_event', onpick)

		plt.show()


	# M1
	def stacked_barchart_mirna_samples_isoforms(self, dataset, mirna_hsa_name):
		samples = dataset.get_samples_by_mirna(mirna_hsa_name)
		isoforms_by_sample = defaultdict(lambda: defaultdict(int))
		for sample in samples:
			isoforms_by_sample[sample.get_name()] = sample.get_isoforms_count(mirna_hsa_name)

		array_dictionary = defaultdict(list)
		for isoform in self.__isoforms:
			for sample in samples:
				if isoform in isoforms_by_sample[sample.get_name()].keys():
					array_dictionary[isoform].append(int(isoforms_by_sample[sample.get_name()][isoform]))
				else:
					array_dictionary[isoform].append(0)
			if sum(array_dictionary[isoform]) == 0:
				del array_dictionary[isoform]  # don't show isoform if not present in any sample

		# sum over columns
		totals = np.asarray(array_dictionary.values()).sum(axis=0)

		height_cumulative = np.zeros(len(samples))

		sample_labels = [sample.get_name() for sample in samples]

		fig_width, rect = Charts.__get_figwidth_rect(len(sample_labels))
		fig = plt.figure(figsize=(fig_width, Constants.FIGURE_H))
		ax1 = fig.add_axes(rect)
		ax2 = ax1.twinx()

		bar_width = Constants.BAR_WIDTH
		tickpositions = np.arange(len(sample_labels))
		xlim = len(sample_labels) - bar_width
		ax1.set_xlim(-bar_width, xlim)
		ax2.set_xlim(-bar_width, xlim)
		ax1.set_xticks(tickpositions)
		ax1.set_xticklabels(sample_labels, size='small', rotation=40, ha='right', picker=True)

		bars = defaultdict()
		keys = array_dictionary.keys()
		colors = dict((key, self.__isoform_colors[key]) for key in keys)
		for i in xrange(0, len(keys)):
			relative_data = [m * 100.0 / n for m, n in zip(array_dictionary[keys[i]], totals)]
			bars[i] = ax1.bar(tickpositions, relative_data, bottom=height_cumulative, width=bar_width, color=colors[keys[i]], edgecolor='white', align='center')
			height_cumulative += relative_data

		ax1.set_ylabel("Percent (%)")
		ax1.set_ylim(0, 110)
		ax1.yaxis.set_ticks_position('left')

		log_totals = np.log10(totals)
		plot2 = ax2.plot(tickpositions, log_totals, "s", markersize=6, color=Constants.DOT_COLOR)
		ax2.set_ylabel(r'$log_{10}$ (absolute values)')
		ax2.set_ylim(0, max(log_totals) + 2)
		ax2.yaxis.set_ticks_position('right')
		ax2.grid()

		legend1 = ax1.legend([p[0] for p in bars.values()], keys, bbox_to_anchor=(1.07, 1), loc=2, borderaxespad=0.)
		legend2 = ax2.legend([plot2[0]], ["abs. value (log scale)"], bbox_to_anchor=(1.07, 0.3), borderaxespad=0., loc=3)
		legend1.draggable(state=True)
		legend2.draggable(state=True)

		legend_patches = defaultdict()
		for label, patch in zip(legend1.get_texts(), legend1.get_patches()):
			legend_patches[patch] = label.get_text()

		plt.xlabel("Samples")
		plt.xlim([min(tickpositions) - bar_width, max(tickpositions) + 2 * bar_width])
		plt.title(mirna_hsa_name + " isoforms %")
		fig.canvas.set_window_title(mirna_hsa_name + " isoforms %")

		fig.autofmt_xdate()

		def on_pick(event):
			_selected_isoform = None

			if event.artist is None:
				return

			_a = event.artist
			if isinstance(_a, Legend):
				for r in _a.hitlist(event.mouseevent):
					if isinstance(r, Rectangle) and r in legend_patches.keys():
						_selected_isoform = legend_patches[r]
						break
					elif isinstance(r, Text) and r.get_text() in legend_patches.values():
						_selected_isoform = r.get_text()
						break
				if _selected_isoform is not None:
					self.barchart_mirna_isoform(dataset, mirna_hsa_name, _selected_isoform, array_dictionary[_selected_isoform], totals)
				return
			elif isinstance(_a, Text) and _a.get_text() in sample_labels:
				self.table_sample_tag_quality(dataset, _a.get_text(), mirna_hsa_name)
				return

		legend1.figure.canvas.mpl_connect('pick_event', on_pick)

		plt.show()


	# T1
	def table_sample_tag_quality(self, dataset, sample_name, mirna_hsa_name, what=None, param=None):

		def build_table(_page):
			colLabels = ("tag index", "tag sequence", "tag quality", "count")
			start = Constants.TAGS_PER_PAGE * _page
			end = min(start + Constants.TAGS_PER_PAGE, len(cellText))
			cellText_subset = [("", "", "", "")] + cellText[start:end]
			colors = [[Constants.WHITE for c in range(4)] for r in range(len(cellText_subset))]

			plt.subplot(2, 1, 1)
			plt.cla()
			plt.axis('off')
			plt.title("tag sequence, quality and count\nmiRNA " + mirna_hsa_name + (" " + param if param is not None else "") + (" in sample " + sample_name if sample_name is not None else ""))
			fig.canvas.set_window_title("tag sequence, quality and count\nmiRNA " + mirna_hsa_name + (" " + param if param is not None else "") + (" in sample " + sample_name if sample_name is not None else ""))

			tab = plt.table(cellText = cellText_subset,
			                rowLabels = [" " for t in cellText_subset],
							colLabels = colLabels,
							colWidths = [0.08, 0.42, 0.42, 0.08],
							rowColours = [Constants.A_COLOR] * len(cellText_subset),
							colColours = [Constants.A_COLOR] * 4,
							cellColours = colors,
							cellLoc = 'left',
							loc = 'upper center')

			tab.auto_set_font_size(False)
			tab.set_fontsize(10)

		tags = deque()
		if sample_name is not None:
			sample = dataset.get_samples()[sample_name]
			tags = [tag for tag in sample.get_tags() if tag.get_mirna().get_hsa_name() == mirna_hsa_name]
		else:
			for sample in dataset.get_samples().values():
				tags.extend([tag for tag in sample.get_tags() if tag.get_mirna().get_hsa_name() == mirna_hsa_name])
		if what is not None:
			if param is not None:
				if param in self.__isoforms:
					tags = [tag for tag in tags if tag.get_isoform() == param]
				elif param in self.__interaction_sites:
					tags = [tag for tag in tags if tag.get_interaction_site() == param]
		pages = int(math.ceil(float(len(tags))/Constants.TAGS_PER_PAGE))
		page = 0

		fig = plt.figure(figsize=(Constants.FIGURE_W_XS, Constants.FIGURE_H_XS), dpi=120)

		cellText = [(int(tag.get_index()), tag.get_sequence(), tag.get_quality(), int(tag.get_count())) for tag in tags]
		# cellText = sorted(cellText, key=itemgetter(2), reverse=True)  # sort by tag count
		cellText = sorted(cellText, key=itemgetter(0), reverse=False)  # sort by tag index

		build_table(page)  # starts with page=0

		plt.subplot(2, 1, 2)
		plt.axis('off')

		prev_bbox_props = dict(boxstyle="larrow, pad=0.5", fc=Constants.A_COLOR, ec="black", lw=1)
		t_prev = plt.text(0, 0.3, "prev", ha="right", va="bottom", rotation=0, size=8, bbox=prev_bbox_props)
		prev_patch = t_prev.get_bbox_patch()
		prev_patch.set_picker(True)

		curr_bbox_props = dict(boxstyle="round, pad=0.5", fc="None", ec="black", lw=0)
		t_curr = plt.text(0.5, 0.3, str(page + 1) + " / " + str(pages), ha="right", va="bottom", rotation=0, size=8, bbox=curr_bbox_props)

		next_bbox_props = dict(boxstyle="rarrow, pad=0.5", fc=Constants.A_COLOR, ec="black", lw=1)
		t_next = plt.text(1, 0.3, "next", ha="right", va="bottom", rotation=0, size=8, bbox=next_bbox_props)
		next_patch = t_next.get_bbox_patch()
		next_patch.set_picker(True)

		export_bbox_props = dict(boxstyle="square, pad=0.5", fc="None", ec="black", lw=1)
		t_export = plt.text(1, 0, Constants.EXPORT, ha="right", va="bottom", rotation=0, size=12, bbox=export_bbox_props)
		export_patch = t_export.get_bbox_patch()
		export_patch.set_picker(True)

		def on_button_pressed(event):
			if export_patch.contains_point((event.x, event.y)):
				print "exporting"
				if self.__export_folder is None:
					self.__export_folder = str(self.__working_folder) + "/" + str(dataset.get_dataset_name()) + "_export"
				if not os.path.exists(self.__export_folder):
					os.makedirs(self.__export_folder)

				if param is None:
					file_name = sample_name + "_" + mirna_hsa_name + "_tags.csv"
				else:
					file_name = sample_name + "_" + mirna_hsa_name + "_" + param + "_tags.csv"
				file_name = os.path.join(self.__export_folder, file_name)

				with open(file_name, 'w') as fp:
					fp.write("\"tag index\",\"tag sequence\",\"tag quality\",count\r\n")
					for index, sequence, quality, count in cellText:
						fp.write(str(index) + "," + sequence + "," + quality + "," + str(count) + "\r\n")
				print "export done"
				return

			pp = t_curr.get_text().split("/")
			page = int(pp[0])
			pages = int(pp[1])
			if next_patch.contains_point((event.x, event.y)):
				if page < pages:
					print "next clicked"
					page += 1
			elif prev_patch.contains_point((event.x, event.y)):
				if page > 1:
					print "prev clicked"
					page -= 1
			t_curr.set_text(str(page) + " / " + str(pages))
			build_table(page - 1)
			fig.canvas.draw()
			fig.canvas.flush_events()

		fig.canvas.mpl_connect('button_press_event', on_button_pressed)
		fig.tight_layout()

		plt.axis('off')
		plt.show()


	# M2
	def barchart_mirna_isoform(self, dataset, mirna_hsa_name, isoform_name, isoform_count_by_sample, totals): # isoform_count_by_sample e' array_dictionary[isoforms[isoform_name]]
		samples = dataset.get_samples_by_mirna(mirna_hsa_name)
		sample_labels = [sample.get_name() for sample in samples]
		color = self.__isoform_colors[isoform_name]

		sample_labels, isoform_count_by_sample, totals = zip(*[(l, c, t) for l, c, t in zip(sample_labels, isoform_count_by_sample, totals) if c > 0])

		fig_width, rect = Charts.__get_figwidth_rect(len(sample_labels))
		fig = plt.figure(figsize=(fig_width, Constants.FIGURE_H))
		ax1 = fig.add_axes(rect)
		ax2 = ax1.twinx()

		bar_width = Constants.BAR_WIDTH
		tickpositions = np.arange(len(sample_labels))
		xlim = len(sample_labels) - bar_width
		ax1.set_xlim(-bar_width, xlim)
		ax2.set_xlim(-bar_width, xlim)
		ax1.set_xticks(tickpositions)
		ax1.set_xticklabels(sample_labels, size='small', rotation=40, ha='right', picker=True)

		relative_data = [m * 100.0 / n for m, n in zip(isoform_count_by_sample, totals)]
		bars = ax1.bar(tickpositions, relative_data, width=bar_width, color=color, edgecolor='white', align='center')
		legend1 = ax1.legend([bars[0]], [isoform_name], bbox_to_anchor=(1.07, 1), borderaxespad=0., loc=2)
		legend1.draggable(state=True)

		ax1.set_ylabel("Percent (%)")
		ax1.set_ylim(0, max(relative_data) * 1.1)
		ax1.yaxis.set_ticks_position('left')

		log_data = np.where(isoform_count_by_sample > 0, np.log10(isoform_count_by_sample), 0)  # warning: log gets called before checking; it works fine anyway
		plot2 = ax2.plot(tickpositions, log_data, "s", markersize=6, color=Constants.DOT_COLOR)
		legend2 = ax2.legend([plot2[0]], [isoform_name + " \n\nabsolute value (log scale)"], bbox_to_anchor=(1.07, 0.7), borderaxespad=0., loc=3)
		legend2.draggable(state=True)

		ax2.set_ylabel(r'$log_{10}$ (absolute values)')
		ax2.set_ylim(0, max(log_data) + 2)
		ax2.yaxis.set_ticks_position('right')
		ax2.grid()

		def on_pick(event):
			if event.artist is None:
				return
			_a = event.artist
			if isinstance(_a, Text):
				if _a.get_text() in sample_labels:
					self.table_sample_tag_quality(dataset, _a.get_text(), mirna_hsa_name, Constants.ISOFORMS, isoform_name)
					return
			elif isinstance(_a, Legend):
				self.table_sample_tag_quality(dataset, None, mirna_hsa_name, Constants.ISOFORMS, isoform_name)
				return

		fig.canvas.mpl_connect('pick_event', on_pick)

		plt.xlabel("Samples")
		plt.title(mirna_hsa_name + " " + isoform_name + " %")
		fig.canvas.set_window_title(mirna_hsa_name + " " + isoform_name + " %")
		fig.autofmt_xdate()
		plt.show()
