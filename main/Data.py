# -*- coding: utf-8 -*-

from collections import deque
from copy import deepcopy
from itertools import takewhile, repeat
import os
import timeit
from model import Constants
from model.Dataset import Dataset
from model.Mirna import Mirna
from model.Sample import Sample
from model.Tag import Tag
from ConfigParser import SafeConfigParser, ParsingError
import numpy as np

__author__ = 'lg'


class Data(object):
	def __init__(self, dataset_original_path_name, working_folder):

		if not os.path.exists(dataset_original_path_name):
			raise IOError("Directory does not exist")

		self.__dataset_name = os.path.basename(dataset_original_path_name)
		self.__dataset_working_folder = working_folder + "/" + self.__dataset_name

		self.__parser = SafeConfigParser()
		self.__parser.read(Constants.CONFIG_FILE_NAME)

		self.__dataset = None
		self.__isoform_map = self.__read_isoforms_config()
		self.__interaction_site_map = self.__read_interaction_sites_config()

		# merge files and build working folder only if needed (it doesn't exist yet)
		if not os.path.exists(self.__dataset_working_folder):
			os.makedirs(self.__dataset_working_folder)
			self.__merge_files(dataset_original_path_name)

		try:
			self.__build_data_structure()
		except Exception as e:
			raise e

	def __merge_files(self, dataset_original_path_name):
		startinglevel = dataset_original_path_name.count(os.sep)
		for top, dirs, files in os.walk(dataset_original_path_name, topdown=True):
			level = top.count(os.sep) - startinglevel
			if level == 1:
				# print("merging files in ", os.path.basename(top))
				sample_name = os.path.basename(top)
				merged_file_name = os.path.join(self.__dataset_working_folder, sample_name + Constants.TXTFILE)
				src_path = os.path.join(top, os.listdir(top).pop(0))
				src_unique = os.path.join(src_path, Constants.UNIQUE_FILE_NAME)
				src_ambigue_selected = os.path.join(src_path, Constants.AMBIGUE_FILE_NAME)

				src_mapped = os.path.join(src_path, Constants.UNIQUE_AMBIGUE_FILE_NAME)
				mapped_reads = np.loadtxt(src_mapped, skiprows=1, usecols=(2,))
				mapped_reads = np.sum(mapped_reads, dtype=np.int)

				src_raw = os.path.join(dataset_original_path_name, "..", Constants.RAW_FASTQ, sample_name + Constants.RAW_FASTQ_EXT)
				if os.path.isfile(src_raw):
					with open(src_raw, "r") as f:
						raw = sum(bl.count("\n") for bl in self.rawbigcount2(f)) / 4
				else:
					raw = 0

				src_adapter_removed = os.path.join(dataset_original_path_name, "..", Constants.AR_FASTQ, sample_name + Constants.AR_FASTQ_EXT)
				if os.path.isfile(src_adapter_removed):
					with open(src_adapter_removed, "r") as f:
						adapter_removed = sum(bl.count("\n") for bl in self.rawbigcount2(f)) / 4
				else:
					adapter_removed = 0

				read_count_header = "mapped_reads " + str(mapped_reads) + ", raw " + str(raw) + ", adapter_removed " + str(adapter_removed)
				print read_count_header

				self.__merge(src_unique, src_ambigue_selected, merged_file_name, read_count_header)
		print "__merge_files done"
		return

	@staticmethod
	def rawbigcount2(files, size=65536):
		while True:
			b = files.read(size)
			if not b:
				break
			yield b

	@staticmethod
	def rawbigcount1(filename):
		with open(filename, 'rb') as f:
			bufgen = takewhile(lambda x: x, (f.read(1024 * 1024) for _ in repeat(None)))
			return sum(buf.count(b'\n') for buf in bufgen if buf)

	@staticmethod
	def __merge(src_unique, src_ambigue_selected, merged_file_name, read_count_header):
		print "merging to ", merged_file_name
		missing_columns = ["0", "0", "0"]

		with open(src_ambigue_selected, "r") as fp:
			merged_lines = fp.readlines()

		with open(src_unique, "r") as fp:
			lines = fp.readlines()
			lines.pop(0)  # discard header

			for src_line in lines:
				src_line = src_line.split("\t")
				dest_line = src_line[0:15] + missing_columns + src_line[15:]
				merged_lines.append('\t'.join(dest_line))

		with open(merged_file_name, 'w') as fp:
			fp.write(str(read_count_header) + "\n")
			fp.writelines(merged_lines)

		print merged_file_name, " merged"

	def __read_isoforms_config(self):
		try:
			isoforms = dict()
			for isoform in self.__parser.options("isoforms"):
				isoforms[int(self.__parser.get("isoforms", isoform))] = isoform
			print "isoforms: ", isoforms
			return isoforms
		except ParsingError, err:
			print 'Could not parse:', err

	def __read_interaction_sites_config(self):
		try:
			int_sites = dict()
			for int_site in self.__parser.options("interaction sites"):
				int_sites[int(self.__parser.get("interaction sites", int_site))] = int_site
			print "interaction sites: ", int_sites
			return int_sites
		except ParsingError, err:
			print 'Could not parse:', err

	def __build_data_structure(self):
		self.__dataset = Dataset(self.__dataset_name)
		print "dataset:", self.__dataset.get_dataset_name()
		# print("#datasets = %r" % Dataset.get_dataset_count())
		samples = deque()

		for f in os.listdir(self.__dataset_working_folder):
			if os.path.isfile(os.path.join(self.__dataset_working_folder, f)):
				sample = Sample(os.path.splitext(f)[0], self.__dataset)
				# TODO sample.set_info()...
				# print("sample name =", sample.get_name())
				# files.append(f)
				# print(os.path.basename(f))
				with open(os.path.join(self.__dataset_working_folder, f)) as infile:
					read_count_header = next(infile)
					read_count_header = read_count_header.split(",")
					sample.set_mapped_reads(int(read_count_header[0].split()[1]))
					sample.set_raw(int(read_count_header[1].split()[1]))
					sample.set_adapter_removed(int(read_count_header[2].split()[1]))
					next(infile)
					header_line = next(infile)  # discard header line

					iso_index = list()
					for i in self.__parser.options("isoform index"):
						iso_index.append(int(self.__parser.get("isoform index", i)))

					int_site_index = list()
					for i in self.__parser.options("interaction site index"):
						int_site_index.append(int(self.__parser.get("interaction site index", i)))

					for line in infile:

						tokens = line.rstrip('\n').split("\t")

						# build isoform code
						isoform_string = ""
						for i in iso_index:
							isoform_string += tokens[i]
						isoform_code = int(isoform_string, 2)

						# build interaction site code
						interaction_site_string = ""
						for i in int_site_index:
							interaction_site_string += tokens[i]
						interaction_site_code = int(interaction_site_string, 2)

						mirna_names = tokens[Constants.MIRNA_NAME]
						mirna_names = mirna_names[1:]
						mirna_names = mirna_names.split()
						mirna = Mirna(tokens[Constants.MIRNA_ID], mirna_names[0], mirna_names[1], tokens[Constants.MIRNA_SEQUENCE], tokens[Constants.SEED_INDEX])

						# index, seq, qual, count, iso, isite, mirna
						tag = Tag(tokens[Constants.TAG_INDEX], tokens[Constants.TAG_SEQUENCE], tokens[Constants.TAG_QUALITY], tokens[Constants.COUNT_TAGS],
						          self.__isoform_map.get(isoform_code), self.__interaction_site_map[interaction_site_code], mirna)

						tag.set_begin_ungapped_mirna(tokens[Constants.BEGIN_UNGAPPED_MIRNA])
						tag.set_begin_ungapped_tag(tokens[Constants.BEGIN_UNGAPPED_TAG])

						sample.add_tag(tag)
					print "#tags in sample", sample.get_name(), ": %r" % sample.get_tags_count()
				samples.append(sample)

		print "#samples in dataset", self.__dataset.get_dataset_name(), "= %r" % Sample.get_samples_count()
		print "#miRNA in dataset", self.__dataset.get_dataset_name(), ": %r" % Mirna.get_mirnas_count()
		self.__dataset.add_samples(samples)
		print "__build_data_structure done"

	def get_dataset(self):
		return self.__dataset
