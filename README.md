**Bioinformatics course project, ay 2015-2016**
# Interactive charts for isoMIR-SEA

GUI application for filtering, collapsing, plotting and navigating charts for single and multiple datasets produced in output by **isomiR-SEA**, a software for NGS expression level extraction.

# How-to
## Install
1. Open a terminal window
2. clone the project: `git clone https://leonard0g@bitbucket.org/leonard0g/interactive_charts_isomirsea.git`
3. `cd` to the cloned folder
4. execute `chmod +x`

## Launch
1. Double-click `projectf` and confirm execution

  **or**

1. open a terminal window
2. `cd` to the cloned folder
3. execute `./projectf`